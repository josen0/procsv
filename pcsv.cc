/*
	PROCSV v1.0

	Copyright (C) 2021 J. Nieto, all rights reserved.
	This program is free software; you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
   	the Free Software Foundation; either version 3, or (at your option)
   	any later version.

   	This program is distributed in the hope that it will be useful,
   	but WITHOUT ANY WARRANTY; without even the implied warranty of
   	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   	GNU General Public License for more details.

 	You should have received a copy of the GNU General Public License
    	along with this program.  If not, see <https://www.gnu.org/licenses/>.

	contact: kkeezff@hotmail.com

*/

#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <sstream>
#include <cstdbool>
#include <cstdint>
#include <cstdio>
#include <algorithm>


using std::fstream;
using std::cout;
using std::cin;
using std::ios;
using std::stringstream;
using std::endl;
using std::string;
using std::vector;
using std::sort;
using std::to_string;
typedef struct
{
  string palabra;
  uint32_t coincidencia;
  size_t columna;
}
  palabra_t;

typedef struct
{
  std::vector<size_t> column;
  std::vector<std::string> param;
}
  procesar_t;

int
main (int argc, char *argv[])
{

  char opc_proc = '0', tiene_encabezado = 'n', separador;
  
  vector<string> row;
  vector<string> column;
  vector<string> header;

  //vector de vectores
  vector<vector<palabra_t>> diccionario_maestro;

  palabra_t palabra_tmp;

  vector<palabra_t> coincidencias_tmp;
  
  string line, word, temp, opc, param, separador_s;

  size_t i, j, k, l, m, n, row_max;
  
  fstream f, r;

  procesar_t p;

  bool vacio;

  bool palabra_encontrada = false;

  vector<int32_t> sort_int;
  vector<int32_t> sort_int_lines;

  uint32_t line_counter = 0;
  
  
  separador = ',';
  
  diccionario_maestro.clear();
  sort_int.clear();
  
  if (2 <= argc)
    {
      string separador_s(argv[1]);
      
      if (0 == separador_s.compare("pc"))
	{
	  //cout << argv[1] << endl;
	  separador = ';';
	}
      if (0 == separador_s.compare("c"))
	{
	    //cout << argv[1] << endl;
	    separador = ',';
	}
    }
  
  f.open ("registros.csv", ios::in);
  r.open ("resultado.csv", ios::out);
  
  while (getline(f, line))
    {

      // cout << line << endl;
      
      row.clear();

      /*
       * Lee un renglon completo y lo almacena el un string llamado 'line'
       */
		
      /*
       * Usado para romper palabras
       */	
      stringstream s(line);
  
      // read every column data of a row and
      // store it in a string variable, 'word'
	 
      while (getline(s, word, separador))
	{
	  /*
	   * Agrega los datos de las columnas del renglon al vector
	   */
	  row.push_back(word);
        }

      if ('n' == tiene_encabezado)
	{
	  column.clear();
	  for (i = 0 ; i < row.size() ; i++)
	    {
	      if (!row[i].empty())
		{
		  cout << '\t' << i << ": " << row[i];
		  column.push_back(row[i]);
		}
	    }
	  cout << endl;
	  
	  cout << "¿Es este el encabezado del archivo ? [s/n] >";
	  cin >> tiene_encabezado;
	  cout << endl;
	  	  
	  if  (tiene_encabezado == 's')
	    {

	      /*
	       * Copia el encabezado al vector header
	       *
	       */
	      header.clear();
	      for (i = 0 ; i < row.size() ; i++)
		{
		  header.push_back(row[i]);
		}
	    
	      /*
	       * Guarda al numero maximo de columnas y pregunta por las opciones a procesar
	       */
	      row_max = row.size();
	      cout << "Seleccione los indices de las columnas que desea procesar. p. ej. 0,5,23 o (t)odos >";
	      cin >> opc;
	      cout << endl;

	      /*
	       * Si la opcion es diferente a 't', necesita procesar que opciones ha puesto el usuario
	       */
	    
	      if (opc.compare("t") != 0 )
		{
		  opc_proc = 'X';
		  stringstream c(opc);
		  
		  p.column.clear();
		  p.param.clear();

		  /*
		   * Convierte el parametro opc a los numeros de columnas a procesar
		   */
		  while (getline(c, word, ','))
		    {
		      /*
		       * Revisa si hay parametros para procesar con el numero de columna
		       */
		      if (word.find('{') == string::npos)
			{
			  /*
			   * Si devuelve npos significa que no hay parametros a procesar
			   * en ese caso se debe de agregar el numero de columna al vector
			   * y el vector de parametros debe estar sin opciones
			   */
			  p.column.push_back(size_t(stoi(word,nullptr,10)));
			  p.param.push_back("");
			}
		      else
			{
			  stringstream o(word);
			  getline(o, param, '{');
			  p.column.push_back(size_t(stoi(param,nullptr,10)));
			  getline(o, param, '{');
		          p.param.push_back(param);
			}
		    }
		}
	      else
		{
		  opc_proc = 't';
		}

	      for (i = 0; i < p.column.size(); i++)
		{
		  r << header[p.column[i]];

		  if ( i < (p.column.size() - 1))
		     {
		       r << separador;  
		     }
		}
	      r << endl;
	      goto fin;
	    }
	}

      /*
       * Empieza a procesar el archivo con las opciones de la variable opc
       */

      switch(opc_proc)
	{
	      
	case 't':

	  cout << "|";
	      
	  for (i = 0 ; i < row_max ; i++)
	    {
	      if (!row[i].empty())
		{
		  r <<  row[i];
		}
	      /*
	      else
		{
		  r << "(vacio)";
		}
	      */
	      
	      if( row.size() > i )
		{
		  r << separador;  
		}
	    }  
	  r << endl;

	  line_counter++;
	  
	  break;

	case 'X':

	  cout << "|";
	    		      
	  /*
	   * --------------------------------------------------------
	   * Busca si esta el parametro v, que no imprime las lineas con columna vacia segun haya configurado
	   * --------------------------------------------------------
	   */
	       
	  vacio = false;
	      
	  for (i = 0 ; i < (p.column).size() ; i++)
	    {
		 
	      //cout << "indice: "<< i << "tamano de columna: " << p.column.size() <<  endl;
	      j = p.column[i];
	      
	      if (p.param[i].find("v", 0) != string::npos)
		{
		  if (row[j].empty())
		    {
		      vacio = true;
		    }
		}
	    }
		
	  if(vacio == false)
	    {
	      /*
	       * --------------------------------------------------------
	       */
			
	      //cout << "vacio == false "<<  endl;
	      //cout << "-------------------------------------"<<  endl;
	      /*
	       * --------------------------------------------------------
	       * Busca si esta el parametro c, de cuenta coincidencias
	       * --------------------------------------------------------
	       */

	      for (i = 0 ; i < p.column.size() ; i++)
		{
		  //cout << "indice: "<< i <<  endl;
		  /*
		   * p.column tiene los indices de las columnas a procesar
		   * p.param tiene los parametros de cada columna
		   */
			
		  j = p.column[i];
			
		  //cout << "parametro a procesar: -";
		  
		  //cout << p.param[i] << "-" << endl;
			
		  //cout << "se compara: " << p.param[i] << " con: c" << endl;
			
		  if (p.param[i].find("c", 0) != string::npos)
		    {
		      //cout << "Se encontro C en: " << i << endl;
		      /*
		       * Busca la palabra en el diccionario
		       * Si el diccionario maestro esta vacio, lo inicializa
		       * con la nueva palabra encontrada
		       */
		      if (diccionario_maestro.empty())
			{
			  palabra_tmp.palabra = row[j];
			  palabra_tmp.coincidencia = 1;
			  palabra_tmp.columna = j;
			  coincidencias_tmp.clear();
			  coincidencias_tmp.push_back(palabra_tmp);
			  diccionario_maestro.push_back(coincidencias_tmp);
			}
		      else
			{
			  /*
			   * Si no esta vacio el diccionario, busca la palabra
			   */
			  for (k = 0; k < diccionario_maestro.size(); k++)
			    {
			      //si es la columna correcta
			      if (diccionario_maestro[k][0].columna == j)
				{
				  palabra_encontrada = false;
				  for (l = 0; l < diccionario_maestro[k].size(); l++ )
				    {
				      
				      if (0 == diccionario_maestro[k][l].palabra.compare(row[j]))
					{
					  /*
					   * Si encuentra alguna coincidencia, incrementa
					   * el contador en uno
					   */
					  diccionario_maestro[k][l].coincidencia++;
					  palabra_encontrada = true;
					  break;
					}
				    }
				  if (false == palabra_encontrada)
				    {
				      palabra_tmp.palabra = row[j];
				      palabra_tmp.coincidencia = 1;
				      palabra_tmp.columna = j;
				      diccionario_maestro[k].push_back(palabra_tmp);
				    }
				}  
			    }
			}
		    }
		  
		  /*
		   * --------------------------------------------------------
		   * Busca si esta el parametro si, de sort int
		   * --------------------------------------------------------
		   */
		  
		  
		  if ((p.param[i]).find("si", 0) != string::npos)
		    {
		      //cout << "parametro SI encontrado" << endl;
		      if (!row[j].empty())
			{
			  sort_int.push_back(stoi(row[j],nullptr,10));
			}				
		    }
		  /*
		   * --------------------------------------------------------
		   */
		  
		}
		      
	      for (i = 0 ; i < (p.column).size() ; i++)
		{
		  
		  j = p.column[i];
		  
		  if (!row[j].empty())
		    {
		      r <<  row[j];
		    }
		  /*
		  else
		    {
		      r << "(VACIO)";
		    }
		  */
		  //if ( p.column.size() > j )
		  if ((p.column.size() - 1) > i )
		    {
		      r << separador;  
		    }
		}  
	      
	      r << endl;
	      
	      //cout << "-------------------------------------"<<  endl;
	      line_counter++;
	    }
	  
	  break;
	  
	fin:	    default:	  
	  break;
	}
	    
    }

  /*
   * Imprime cuantas lineas procesó
   */
  cout << endl << "hecho :)" << endl;
  r << line_counter << endl;
  
  r.close();
  f.close();

  if (!diccionario_maestro.empty())
    {
      fstream rc;
      rc.open ("coincidencias.csv", ios::out);
      for (m = 0; m < diccionario_maestro.size(); m++)
	{
	  for (n = 0; n < diccionario_maestro[m].size(); n++)
	    {
	      rc << diccionario_maestro[m][n].palabra << separador << diccionario_maestro[m][n].coincidencia << endl;
	    }
	}
      rc.close();
    }

  if (!sort_int.empty())
    {
      fstream si;
      bool encontrado;
      line_counter = 0;
      sort_int_lines.clear();
      si.open ("sort.csv", ios::out);
      r.open ("resultado.csv", ios::in);
      sort (sort_int.begin(),sort_int.end());
      cout << "Ordenando"  << endl;
      for (i = 0; i < sort_int.size(); i++)
	{
	  cout << "|"; 
	  /*
	   * Abre el archivo de resultados para realizar la busqueda y escribir el archivo SORT
	   */
	  //si << sort_int[i] << endl;

	  while (getline(r, line))
	    {
	      //cout << i <<  ":"<< line << endl;
	      encontrado = false;
	      if (string::npos != line.find(to_string(sort_int[i])))
		{
		  if ( false == sort_int_lines.empty())
		    {
		      for (j = 0; j < sort_int_lines.size(); j++)
			{
			  if (line_counter == sort_int_lines[j])
			    {
			      encontrado = true;
			      //cout << "line_counter = sort_int_lines" << endl;
			      break;
			    }
			}
		    }
		  if (false == encontrado)
		    {
		      si << line << endl;
		      //cout << j << endl;
		      sort_int_lines.push_back(line_counter);
		      //cout << "numero de linea no encotrado, se agrega al vector" << endl;
		    }
		}
	      line_counter++;
	    }
	  r.clear();
	  r.seekg(ios::beg);
	  //getchar();
	}
      r.close();
      si.close();
      cout << endl;

    }
  
      return 0;
}
