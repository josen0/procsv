# PROCSV



## ¿Qué es PROCSV?

PROCSV o -Juanito's report tool- es una herramienta creada para mi uso personal, que me ayudo bastante a generar reportes que me pedían en tiempos de pandemia. Esta herramienta filtra un archivo csv, quitando renglones si un campo este vacío, ordenando renglones tomando como referencia un campo en especifico o contando cuantas coincidencias encuentra en un campo.

La sintaxis es la siguiente: numerodecampo{filtro

Los filtros a usar son: c (coincidencias), v (vacio), si (ordena entero)

Ejemplo: 0{vsi,17{c

Elimina los valores vacíos (v) del campo 0 y ordena usando el valor entero que se tiene en el campo 0.
Cuenta las coincidencias (c) que se encuentran en el campo 17.

PROCSV es funcional y esta bajo licencia de software libre.

Esta herramienta la archivo porque me parece puede ser implementada más fácilmente con algún otro lenguaje como AWK.
